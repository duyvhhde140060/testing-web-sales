package com.example.demo.security;

import com.example.demo.mapper.login.CheckLoginByUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.demo.model.User;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyBatisUserDetailsService implements UserDetailsService {

    @Autowired
    private CheckLoginByUserMapper checkLoginByUserMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // Get userName and password from DB
        User user = checkLoginByUserMapper.getUserName(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(
                user.getUserName(), user.getPassword(),
                true, true, true, true,
                AuthorityUtils.createAuthorityList("ROLE_USER")
        );
    }
}
