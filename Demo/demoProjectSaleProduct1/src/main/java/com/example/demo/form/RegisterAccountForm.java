package com.example.demo.form;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import jakarta.validation.constraints.Size;

@Getter
@Setter
public class RegisterAccountForm {

    @NotEmpty
    private String name;

    @NotEmpty
    @Size(min=10, max=30)
    private String email;

    @NotEmpty
    @Size(min=0, max=30)
    private String password;
}
