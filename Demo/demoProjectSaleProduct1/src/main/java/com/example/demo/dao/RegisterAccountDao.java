package com.example.demo.dao;

import com.example.demo.mapper.signup.RegisterAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Random;

@Repository
public class RegisterAccountDao {

    @Autowired
    RegisterAccountMapper registerAccountMapper;

    public int registerAccount(String name, String email, String password) {
        int randomId = getRandomNumberUsingNextInt(3, 100000);
        return registerAccountMapper.registerAccount(randomId, name, email, password);
    }

    public int getRandomNumberUsingNextInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

}
