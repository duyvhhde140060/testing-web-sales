package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoProjectSaleProduct1Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoProjectSaleProduct1Application.class, args);
	}

}
