package com.example.demo.urlCommon;

import org.springframework.stereotype.Component;

@Component
public class UrlTemplate {

    private UrlTemplate() {
    }

    public static String URL_LOGIN = "com/example/login/index";

    public static String URL_SIGNUP = "com/example/signup/index";

    public static String URL_HOME = "com/example/home/ustora/index";
}
