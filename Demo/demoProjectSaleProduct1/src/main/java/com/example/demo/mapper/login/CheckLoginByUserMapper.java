package com.example.demo.mapper.login;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Get username and password from DB to check login.
 */
@Mapper
public interface CheckLoginByUserMapper {

    /**
     * Get username from DB
     *
     * @param userName input from client
     * @return @{{@link User}}
     */
    User getUserName(@Param("userName") String userName);
}
