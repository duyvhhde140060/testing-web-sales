package com.example.demo.mapper.signup;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RegisterAccountMapper {

    /**
     * This method register to DB
     */
    int registerAccount(int id, String name, String email, String password);
}
