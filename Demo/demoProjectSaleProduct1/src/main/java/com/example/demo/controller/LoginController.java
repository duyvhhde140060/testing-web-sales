package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.demo.urlCommon.UrlTemplate;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String showLoginPage() {
        return UrlTemplate.URL_LOGIN;
    }
}
