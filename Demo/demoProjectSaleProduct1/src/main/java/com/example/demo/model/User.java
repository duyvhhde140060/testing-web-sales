package com.example.demo.model;

public class User {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ManagerAccount.id
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ManagerAccount.user_name
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	private String userName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ManagerAccount.password
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	private String password;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ManagerAccount.role
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	private String role;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ManagerAccount.id
	 * @return  the value of ManagerAccount.id
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ManagerAccount.id
	 * @param id  the value for ManagerAccount.id
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ManagerAccount.user_name
	 * @return  the value of ManagerAccount.user_name
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ManagerAccount.user_name
	 * @param userName  the value for ManagerAccount.user_name
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ManagerAccount.password
	 * @return  the value of ManagerAccount.password
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ManagerAccount.password
	 * @param password  the value for ManagerAccount.password
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ManagerAccount.role
	 * @return  the value of ManagerAccount.role
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	public String getRole() {
		return role;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ManagerAccount.role
	 * @param role  the value for ManagerAccount.role
	 * @mbg.generated  Sat Apr 20 15:46:35 ICT 2024
	 */
	public void setRole(String role) {
		this.role = role;
	}
}