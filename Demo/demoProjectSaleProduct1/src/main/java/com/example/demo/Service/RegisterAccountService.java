package com.example.demo.Service;

import com.example.demo.dao.RegisterAccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisterAccountService {

    @Autowired
    RegisterAccountDao registerAccountDao;

    public int registerAccount (String name, String email, String password) {

        return registerAccountDao.registerAccount(name, email, password);
    }
}
