package com.example.demo.controller;

import com.example.demo.Service.RegisterAccountService;
import com.example.demo.form.RegisterAccountForm;
import com.example.demo.urlCommon.UrlTemplate;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.Map;


/**
 * Register account.
 */
@Controller
public class RegisterterController {

    private final static String SPACE = " ";

    @Autowired
    RegisterAccountService registerAccountService;

    /**
     * Init screen register.
     */
    @GetMapping("/signup")
    public String signUp(Model model) {
        model.addAttribute("RegisterAccountForm", new RegisterAccountForm());
        return UrlTemplate.URL_SIGNUP;
    }

    /**
     * Register account and save to DB.
     */
    @PostMapping("/signup")
    public String registerUser(@Valid RegisterAccountForm registerAccountForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            Map<String, String> fieldErrors = new HashMap<>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                fieldErrors.put(error.getField(), error.getField() + SPACE +  error.getDefaultMessage());
            }

            redirectAttributes.addFlashAttribute("fieldErrors", fieldErrors);

            return "redirect:/signup";
        }

        // Register account
        registerAccountService.registerAccount(registerAccountForm.getName(), registerAccountForm.getEmail(), registerAccountForm.getPassword());
        return "redirect:/login";
    }
}
