package com.example.demo.controller;

import java.util.List;

import com.example.demo.urlCommon.UrlTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;
import com.example.demo.model.UserExample;

@Controller
@PreAuthorize("hasRole('ROLE')")
public class TestConnectController {

	@Autowired
	UserMapper mapper;
	
	@GetMapping("/home")
	public String index() {

		UserExample example = new UserExample();
		List<User> user = mapper.selectByExample();
		
		return UrlTemplate.URL_HOME;
	}
}
